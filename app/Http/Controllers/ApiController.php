<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ApiService;

class ApiController extends Controller
{
    public function infoPrevMacTimestamp(Request $request) {
        $apiService = new ApiService();
        return $apiService->curlGet('/info/prev/', [
            $request->route('mac'),
            $request->route('timestamp')
        ]);
    }

    public function infoMacTimestamp(Request $request) {
        $apiService = new ApiService();
        return $apiService->curlGet('/info/', [
            $request->route('mac'),
            $request->route('timestamp')
        ]);
    }

    public function infoTotalDevices() {
        $apiService = new ApiService();
        return $apiService->curlGet('/info/totalDevices/', []);
    }

    public function infoFirmware() {
        $apiService = new ApiService();
        return $apiService->curlGet('/info/firmware/', []);
    }

    public function activedayMac(Request $request) {
        $apiService = new ApiService();
        return $apiService->curlGet('/activeday/', [
            $request->route('mac')
        ]);
    }

    public function logsLasthourTimestamp(Request $request) {
        $apiService = new ApiService();
        return $apiService->curlGet('/logs/lasthour/', [
            $request->route('timestamp')
        ]);
    }

    public function logsInfoPrevMacTimestamp(Request $request) {
        $apiService = new ApiService();
        return $apiService->curlGet('/logs/info/prev/', [
            $request->route('mac'),
            $request->route('timestamp')
        ]);
    }

    public function logsInfoNextMacTimestamp(Request $request) {
        $apiService = new ApiService();
        return $apiService->curlGet('/logs/info/next/', [
            $request->route('mac'),
            $request->route('timestamp')
        ]);
    }

    public function logsTimestampsMacDayMonthYear(Request $request) {
        $apiService = new ApiService();
        return $apiService->curlGet('/logs/timestamps/', [
            $request->route('mac'),
            $request->route('day'),
            $request->route('month'),
            $request->route('year')
        ]);
    }

    public function logsMacDayMonthYear(Request $request) {
        $apiService = new ApiService();
        return $apiService->curlGet('/logs/', [
            $request->route('mac'),
            $request->route('day'),
            $request->route('month'),
            $request->route('year')
        ]);
    }

    public function eventsInfoPrevMacTimestamp(Request $request) {
        $apiService = new ApiService();
        return $apiService->curlGet('/events/info/prev/', [
            $request->route('mac'),
            $request->route('timestamp')
        ]);
    }

    public function eventsTimestampsMacDayMonthYear(Request $request) {
        $apiService = new ApiService();
        return $apiService->curlGet('/events/timestamps/', [
            $request->route('mac'),
            $request->route('day'),
            $request->route('month'),
            $request->route('year')
        ]);
    }

    public function ips() {
        $apiService = new ApiService();
        return $apiService->curlGet('/ips/', []);
    }

    public function macs() {
        $apiService = new ApiService();
        return $apiService->curlGet('/macs/', []);
    }

    public function macsIpIp(Request $request) {
        $apiService = new ApiService();
        return $apiService->curlGet('/macs/ip/', [
            $request->route('ip')
        ]);
    }

    public function macsAvailTimestamp(Request $request) {
        $apiService = new ApiService();
        return $apiService->curlGet('/macs/avail/', [
            $request->route('timestamp')
        ]);
    }

    public function timerangeMac(Request $request) {
        $apiService = new ApiService();
        return $apiService->curlGet('/timerange/', [
            $request->route('mac')
        ]);
    }

    public function wanMacDayMonthYear(Request $request) {
        $apiService = new ApiService();
        return $apiService->curlGet('/wan/', [
            $request->route('mac'),
            $request->route('day'),
            $request->route('month'),
            $request->route('year')
        ]);
    }

    public function lanMacDayMonthYear(Request $request) {
        $apiService = new ApiService();
        return $apiService->curlGet('/lan/', [
            $request->route('mac'),
            $request->route('day'),
            $request->route('month'),
            $request->route('year')
        ]);
    }

    public function summaryMacDayMonthYear(Request $request) {
        $apiService = new ApiService();
        return $apiService->curlGet('/summary/', [
            $request->route('mac'),
            $request->route('day'),
            $request->route('month'),
            $request->route('year')
        ]);
    }

    public function wifiFreqDayMonthYear(Request $request) {
        $apiService = new ApiService();
        return $apiService->curlGet('/wifi/', [
            $request->route('freq'),
            $request->route('day'),
            $request->route('month'),
            $request->route('year')
        ]);
    }

    public function infoMacDayMonthYear(Request $request) {
        $apiService = new ApiService();
        return $apiService->curlGet('/info/', [
            $request->route('mac'),
            $request->route('day'),
            $request->route('month'),
            $request->route('year')
        ]);
    }

    public function wifiClientsMacDayMonthYear(Request $request) {
        $apiService = new ApiService();
        return $apiService->curlGet('/wifi/clients/', [
            $request->route('mac'),
            $request->route('day'),
            $request->route('month'),
            $request->route('year')
        ]);
    }

    public function systemMacDayMonthYear(Request $request) {
        $apiService = new ApiService();
        return $apiService->curlGet('/system/', [
            $request->route('mac'),
            $request->route('day'),
            $request->route('month'),
            $request->route('year')
        ]);
    }

    public function login() {
        if ($_SERVER['CONTENT_TYPE'] == 'application/json') {
            $json = json_decode(file_get_contents('php://input'));
            if ($json != null) {
                if (array_key_exists('username', $json) && array_key_exists('password', $json)) {
                    $fields = [
                        'username' => $json->username,
                        'password' => md5($json->password)
                    ];
                    $headers = [
                        'Content-Type: application/json'
                    ];
                    $apiService = new ApiService();
                    return $apiService->curlPost('/login', $fields, $headers);
                }
            }
        }
    }
}
