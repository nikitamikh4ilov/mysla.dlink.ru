<?php

namespace App\Services;

class ApiService {
    private function getUrl() {
        return 'http://mysla.dlink.ru:8090';
    }
    
    public function curlGet($method, $params) {
        $headers = getallheaders();
        if ($headers && array_key_exists('Token', $headers)) {
            $headers = [
                'Token:' . $headers['Token']
            ];
            foreach ($params as $param) {
                $method = $method . $param . '/';
            }
            $ch = curl_init($this->getUrl() . $method);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $html = curl_exec($ch);
            curl_close($ch);
            return $html;
        }
    }

    public function curlPost($method, $fields, $headers) {
        $ch = curl_init($this->getUrl() . $method);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $html = curl_exec($ch);
        curl_close($ch);
        return $html;
    }
}
