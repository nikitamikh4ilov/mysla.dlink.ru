<?php

Route::get('/api/info/prev/{mac}/{timestamp}', 'ApiController@infoPrevMacTimestamp');
Route::get('/api/info/{mac}/{timestamp}', 'ApiController@infoMacTimestamp');
Route::get('/api/info/totalDevices', 'ApiController@infoTotalDevices');
Route::get('/api/info/firmware', 'ApiController@infoFirmware');

Route::get('/api/activeday/{mac}', 'ApiController@activedayMac');

Route::get('/api/logs/lasthour/{timestamp}', 'ApiController@logsLasthourTimestamp');
Route::get('/api/logs/info/prev/{mac}/{timestamp}', 'ApiController@logsInfoPrevMacTimestamp');
Route::get('/api/logs/info/next/{mac}/{timestamp}', 'ApiController@logsInfoNextMacTimestamp');
Route::get('/api/logs/timestamps/{mac}/{day}/{month}/{year}', 'ApiController@logsTimestampsMacDayMonthYear');
Route::get('/api/logs/{mac}/{day}/{month}/{year}', 'ApiController@logsMacDayMonthYear');

Route::get('/api/events/info/prev/{mac}/{timestamp}', 'ApiController@eventsInfoPrevMacTimestamp');
Route::get('/api/events/timestamps/{mac}/{day}/{month}/{year}', 'ApiController@eventsTimestampsMacDayMonthYear');

Route::get('/api/ips', 'ApiController@ips');

Route::get('/api/macs', 'ApiController@macs');
Route::get('/api/macs/ip/{ip}', 'ApiController@macsIpIp');
Route::get('/api/macs/avail/{timestamp}', 'ApiController@macsAvailTimestamp');

Route::get('/api/timerange/{mac}', 'ApiController@timerangeMac');

Route::get('api/wan/{mac}/{day}/{month}/{year}', 'ApiController@wanMacDayMonthYear');
Route::get('api/lan/{mac}/{day}/{month}/{year}', 'ApiController@lanMacDayMonthYear');
Route::get('api/summary/{mac}/{day}/{month}/{year}', 'ApiController@summaryMacDayMonthYear');
Route::get('api/wifi/{freq}/{day}/{month}/{year}', 'ApiController@wifiFreqDayMonthYear');
Route::get('api/info/{mac}/{day}/{month}/{year}', 'ApiController@infoMacDayMonthYear');
Route::get('api/wifi/clients/{mac}/{day}/{month}/{year}', 'ApiController@wifiClientsMacDayMonthYear');
Route::get('api/system/{mac}/{day}/{month}/{year}', 'ApiController@systemMacDayMonthYear');

Route::post('/api/login', 'ApiController@login');


